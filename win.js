var winState = {
    create: function () {
        // Add a background image
        var bg = game.add.image(game.width/2, game.height, 'bg2');
        bg.anchor.setTo(0.5, 1);

        // Display the name of the game
        var failLabel = game.add.text(game.width/2, 100, 'Congrats!', { font: '40px Arial', fill: '#ffffff' });
        failLabel.anchor.setTo(0.5, 0.5);

        var scoreLabel = game.add.text(game.width/2, 150, 'Scoreboard.', { font: '30px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);

        var scoreBoard = [];
        var usrname = 'usrname';

        var input = document.createElement("input");
        input.type = "text";
        input.width = "100px";
        input.style.position = "absolute";
        input.style.left = (game.width/2 - 75) + "px";
        input.style.top = 250 + "px";
        input.style.zIndex = 999;
        document.getElementById('canvas').appendChild(input);

        var submit = document.createElement("button");
        submit.innerHTML = "submit";
        submit.style.position = "absolute";
        submit.style.left = (game.width/2 + 75) + "px";
        submit.style.top = 250 + "px";
        submit.style.zIndex = 999;
        document.getElementById('canvas').appendChild(submit);

        submit.addEventListener('click', function () {
            usrname = input.value;

            firebase.database().ref('score').child(usrname).set(game.global.score);
            firebase.database().ref('score').once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (element) {
                    scoreBoard.push({key: element.key, val: element.val()});
                })

                scoreBoard.sort(function (a, b) {
                    return a.val > b.val ? -1 : 1;
                })
        
                for (let index = 0; index < scoreBoard.length && index < 10; index++) {
                    var i = index + 1;
                    var label = game.add.text(game.width/2, 200 + 20 * i, `${i}. ${scoreBoard[index].key}: ${scoreBoard[index].val}`, { font: '20px Arial', fill: '#ffffff' });
                    label.anchor.setTo(0.5, 0.5);
                    if (usrname === scoreBoard[index].key) {
                        label.setStyle({ font: '20px Arial', fill: '#cfff0f' });
                    }
                }
            })
            input.style.display = "none";
            submit.style.display = "none";
        })


        var startLabel = game.add.text(game.width/2, 450, 'Press Enter To Restart.', { font: '20px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);

        // Show the score at the center of the screen

        // Explain how to start the game

        var startKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        startKey.onDown.add(this.start, this);
    },
    start: function () {
        // Start the actual game
        game.global.score = 0;
        game.state.start('play');
    },
}; 