var menuState = {
    create: function () {
        // Add a background image
        var bg = game.add.image(game.width/2, game.height, 'bg2');
        bg.anchor.setTo(0.5, 1);

        // Display the name of the game
        var nameLabel = game.add.text(game.width/2, 200, 'Press Enter To Start.', { font: '30px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);

        // Show the score at the center of the screen

        // Explain how to start the game

        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var startKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        startKey.onDown.add(this.start, this);
    },
    start: function () {
        // Start the actual game
        game.state.start('play');
    },
}; 