var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150, 'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);

        // Display the progress bar

        // Load all game assets
        game.load.image('player', 'assets/player.png');
        game.load.image('enemy', 'assets/enemy.png');
        game.load.image('enemyT0', 'assets/enemyT0.png');
        game.load.image('danmaku', 'assets/danmaku.png');
        game.load.image('danmaku2', 'assets/danmaku2.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('bullet2', 'assets/bullet2.png');
        game.load.image('item', 'assets/item.png');
        game.load.image('ult', 'assets/ult.png');
        game.load.image('bg', 'assets/bg.jpg');
        game.load.image('bg2', 'assets/bg2.jpg');

        game.load.image('enemyT0Wrap', 'assets/enemyT0_wrap.png');
        game.load.spritesheet('playerWrap', 'assets/player_wrap.png', 34, 44);
        game.load.spritesheet('enemyWrap', 'assets/enemy_wrap.png', 168, 100);

        game.load.audio('hit', 'assets/hit.wav');
        game.load.audio('bgm', 'assets/bgm.mp3');
        
        // Load a new asset that we will use in the menu state
        
    },
    create: function() {
        // Go to the menu state
        game.state.start('menu');
    }
}; 