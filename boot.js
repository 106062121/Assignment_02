var bootState = {
    preload: function () {
        // Load the progress bar image.

    },
    create: function () {
        // Set some game settings.
        //game.world.setBounds(0, -1000, 650, 1500);
        game.stage.backgroundColor = '#000000';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        // Start the load state.
        game.state.start('load');
    }
}; 