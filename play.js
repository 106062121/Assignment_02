var enemyObj = function () {
    this.danmaku = [];
}

var playState = {
    preload: function () {

    },
    create: function () {
        /// bg
        //this.bg = game.add.image(game.width/2, game.height, 'bg');
        this.bg = game.add.tileSprite(0, 0, game.width, game.height, 'bg')

        /// control keys
        this.cursor = game.input.keyboard.createCursorKeys();
        this.attackKey = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.ultKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
        this.pauseKey = game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.pauseKey.onDown.add(this.pause, this)

        /// player
        this.playerWrap = game.add.sprite(game.width/2, game.height/2, 'playerWrap');
        this.playerWrap.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.playerWrap);
        this.playerWrap.frame = 0;
        this.playerWrap.animations.add('goFront', [0, 1, 2], 8, true);
        this.playerWrap.animations.add('goBack', [0], 8, true);
        this.playerWrap.animations.add('goLeft', [6, 7, 8], 8, true);
        this.playerWrap.animations.add('goRight', [3, 4, 5], 8, true);
        this.playerWrap.animations.play('goFront');
        this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        this.player.anchor.setTo(0.5, 0.5);
        this.player.alpha = 0;
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;
        this.player.life = 10;
        this.player.ult = 3;
        this.player.attack = false;

        /// Particle
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;

        /// sound
        this.hitSound = game.add.audio('hit');
        this.hitSound.volume = 0.5;
        this.backgroundSound = game.add.audio('bgm');
        this.backgroundSound.volume = 0.5;
        this.backgroundSound.play();

        /// menu label
        this.playerLifeLabel = game.add.text(0, 0, 'Player Life: xx', { font: '10px Arial', fill: '#ffffff' });
        this.playerLifeLabel.anchor.setTo(0, 0);
        this.playerUltLabel = game.add.text(0, 10, 'Ult Left: xx', { font: '10px Arial', fill: '#ffffff' });
        this.playerUltLabel.anchor.setTo(0, 0);
        this.enemyLifeLabel = game.add.text(game.width, 10, '', { font: '10px Arial', fill: '#ffffff' });
        this.enemyLifeLabel.anchor.setTo(1, 0);
        this.levelLabel = game.add.text(game.width, 0, 'Level: xx', { font: '10px Arial', fill: '#ffffff' });
        this.levelLabel.anchor.setTo(1, 0);
        this.pauseLabel = game.add.text(game.width/2, 150, 'Game Pasued.', { font: '30px Arial', fill: '#ffffff' });
        this.pauseLabel.anchor.setTo(0.5, 0.5);
        this.pauseLabel.kill()

        /// pause menu
        this.scale_box = document.createElement("input");
        this.scale_box.type = "range";
        this.scale_box.className = "slider";
        this.scale_box.width = "100px";
        this.scale_box.style.position = "absolute";
        this.scale_box.style.left = (game.width/2 - 75) + "px";
        this.scale_box.style.top = 250 + "px";
        this.scale_box.min = 0;
        this.scale_box.max = 100;
        this.scale_box.value = 50;
        this.scale_box.style.zIndex = 999;
        this.scale_box.style.display = "none";
        document.getElementById('canvas').appendChild(this.scale_box);

        /// item
        this.item = [1, 1, 1];
        this.enhance = game.add.sprite(game.width/2, game.height/2, 'item');
        this.enhance.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.enhance);
        this.enhance.kill()

        /// danmaku
        this.bullet = [];
        this.bulletAlt = [];
        this.bulletAlt2 = [];
        this.ult = [];

        ///enemy
        this.enemyT0 = [];

        /// temp
        

        /// setting
        this.timer = 0;
        this.level = 1;
        this.lvltime = 600;
        //game.camera.follow(this.player);
    },
    update: function () {
        /// clk divider
        this.timer += 1;

        if (this.timer % 8 === 0) this.update_at_8();
        if (this.timer % 4 === 0) this.update_at_15();

        /// movement
        this.movePlayer();
        this.bg.tilePosition.y += 1;

        if (this.level === 1) {
            this.moveEnemyT0();
        }
        if (this.level === 2) {
            this.enemyDanmakuMove();
        }

        /// collision
        if (this.level === 1) {
            this.enemyT0.forEach(function (Enemy) {
                Enemy.danmaku.forEach(function (danmaku, index) {
                    if (game.physics.arcade.overlap(this.player, danmaku)) {
                        this.player.life -= 1;
                        danmaku.destroy();
                        Enemy.danmaku.splice(index, 1);
                    }
                    else if (!danmaku.inWorld) {
                        Enemy.danmaku.splice(index, 1);
                    }
                }, this)
            }, this)
        }
        if (this.level === 2) {
            this.enemy.danmaku.forEach(function (danmaku, index) {
                if (game.physics.arcade.overlap(this.player, danmaku)) {
                    this.player.life -= 1;
                    danmaku.destroy();
                    this.enemy.danmaku.splice(index, 1);
                }
                else if (!danmaku.inWorld) {
                    this.enemy.danmaku.splice(index, 1);
                }
            }, this);
        }

        this.bullet.forEach(function (bullet, index) {
            if (this.level === 1) {
                this.enemyT0.forEach(function (Enemy) {
                    if (game.physics.arcade.overlap(Enemy.sprite, bullet)) {
                        //this.particleEnemy();
                        this.hitSound.play();
                        Enemy.life -= 1;
                        bullet.destroy();
                        this.bullet.splice(index, 1);
                    }
                    /*else if (bullet.y < 0) {
                        this.bullet.splice(index, 1);
                    }*/

                    if (Enemy.life < 1) {
                        this.destroyEnemyT0(Enemy);
                    }
                }, this)
            }
            if (this.level === 2) {
                if (game.physics.arcade.overlap(this.enemy.sprite, bullet)) {
                    //this.particleEnemy();
                    this.hitSound.play();
                    this.enemy.life -= 1;
                    bullet.destroy();
                    this.bullet.splice(index, 1);
                } 
                /*else if (bullet.y < 0) {
                    this.bullet.splice(index, 1);
                }*/
            }
        }, this);

        this.bulletAlt.forEach(function (bullet, index) {
            if (this.level === 1) {
                this.enemyT0.forEach(function (Enemy) {
                    if (game.physics.arcade.overlap(Enemy.sprite, bullet)) {
                        //this.particleEnemy();
                        this.hitSound.play();
                        Enemy.life -= 1;
                        bullet.destroy();
                        this.bullet.splice(index, 1);
                    }
                    /*else if (bullet.y < 0) {
                        this.bullet.splice(index, 1);
                    }*/

                    if (Enemy.life < 1) {
                        this.destroyEnemyT0(Enemy);
                    }
                }, this)
            }
            if (this.level === 2) {
                if (game.physics.arcade.overlap(this.enemy.sprite, bullet)) {
                    //this.particleEnemy();
                    this.hitSound.play();
                    this.enemy.life -= 1;
                    bullet.destroy();
                    this.bullet.splice(index, 1);
                } 
                /*else if (bullet.y < 0) {
                    this.bullet.splice(index, 1);
                }*/
            }
        }, this);

        this.bulletAlt2.forEach(function (bullet, index) {
            if (this.level === 1) {
                this.enemyT0.forEach(function (Enemy) {
                    if (game.physics.arcade.overlap(Enemy.sprite, bullet)) {
                        //this.particleEnemy();
                        this.hitSound.play();
                        Enemy.life -= 1;
                        bullet.destroy();
                        this.bullet.splice(index, 1);
                    }
                    /*else if (bullet.y < 0) {
                        this.bullet.splice(index, 1);
                    }*/

                    if (Enemy.life < 1) {
                        this.destroyEnemyT0(Enemy);
                    }
                }, this)
            }
            if (this.level === 2) {
                if (game.physics.arcade.overlap(this.enemy.sprite, bullet)) {
                    //this.particleEnemy();
                    this.hitSound.play();
                    this.enemy.life -= 1;
                    bullet.destroy();
                    this.bullet.splice(index, 1);
                } 
                /*else if (bullet.y < 0) {
                    this.bullet.splice(index, 1);
                }*/
            }
        }, this);

        this.ult.forEach(function (ult, num) {
            if (this.level === 1) {
                this.enemyT0.forEach(function (Enemy) {
                    Enemy.danmaku.forEach(function (danmaku, index) {
                        if (game.physics.arcade.overlap(ult, danmaku)) {
                            danmaku.destroy();
                            Enemy.danmaku.splice(index, 1);

                            ult.destroy();
                            this.ult[num] = null;
                        }
                        else if (!danmaku.inWorld) {
                            Enemy.danmaku.splice(index, 1);
                        }
                    }, this)
                }, this)
            }
            if (this.level === 2) {
                this.enemy.danmaku.forEach(function (danmaku, index) {
                    if (game.physics.arcade.overlap(ult, danmaku)) {
                        danmaku.destroy();
                        this.enemy.danmaku.splice(index, 1);

                        ult.destroy();
                        this.ult[num] = null;
                    }
                    else if (!danmaku.inWorld) {
                        this.enemy.danmaku.splice(index, 1);
                    }
                }, this);
            }
        }, this);

        /// menu label update
        this.playerLifeLabel.text = `Player Life: ${this.player.life}`;
        this.playerUltLabel.text = `Ult Left: ${this.player.ult}`;
        this.levelLabel.text = `Level: ${this.level}`;

        /// state judgement
        if (this.player.life < 1) {
            this.playerDie();
        }

        if (this.level === 1) {
            this.enemyT0.forEach(function (Enemy, index) {
                if (Enemy.life < 1) {
                    this.destroyEnemyT0(Enemy);
                    this.enemyT0.splice(index, 1)
                }
            }, this)
        }
        if (this.level === 2) {
            this.enemyT0.forEach(function (Enemy, index) {
                    this.destroyEnemyT0(Enemy);
                    this.enemyT0.splice(index, 1)
            }, this)

            this.enemyLifeLabel.text = `Enemy Life: ${this.enemy.life}`;
            if (this.enemy.life < 1) {
                this.destroyEnemy();
                this.playerWin();
            }
        }

        if (this.timer === this.lvltime) {
            this.level = 2;
            this.enemyCreate();
        }

        ///temp
        this.aimbot();
        if (game.physics.arcade.overlap(this.player, this.enhance)) {
            this.enhance.kill();
            var i = Math.floor((Math.random() * 3));
            //this.item[i] += 1;
            this.item[0] += 1;
            this.item[1] += 1;
            this.item[2] += 1;
        }
    
    },
    update_at_8: function () {
        if (this.level === 1) {
            if (this.timer % 10 === 0) {
                this.enemyCreateT0();
            }

            if (this.timer % 30 === 0) {
                if (Math.random() > 0.5 || 1) {
                    if (this.enhance.exists) {
                        this.enhance.kill();
                    }
                    this.enhance.revive();
                    this.enhance.x = game.width/2;
                    this.enhance.y = 0;
                    this.enhance.body.velocity.x = Math.random() * 100;
                    this.enhance.body.velocity.y = Math.random() * 100;
                }
            }

            this.enemyT0.forEach(function (Enemy) {
                this.enemyAttackT0(Enemy);
            }, this)
        }
        else if (this.level === 2) {
            if (this.enemy.sprite.exists) {
                if (this.timer % 60 === 0) this.moveEnemy();
            }
        }

        if (this.player.attack) {
            if (this.item[1]) {
                this.playerAttackAlt();
            }
            if (this.item[2]) {
                this.playerAttackAlt2();
            }
        }

        if (this.timer % 100 === 0) {
            if (this.item[0]) {
                this.item[0] -= 1;
            } 
            if (this.item[1]) {
                this.item[1] -= 1;
            } 
            if (this.item[2]) {
                this.item[2] -= 1;
            }
        }
    },
    update_at_15: function () {
        if (this.player.attack) {
            this.playerAttack();
            //this.playerAttackAlt();
            //this.playerAttackAlt2();
        }
        
        if (this.level === 2) {
            this.enemyAttack();
        }
    },
    pause: function () {
        if (game.paused) {
            game.paused = false;
            this.pauseLabel.kill();

            this.scale_box.style.display = "none";
        }
        else {
            game.paused = true;
            this.pauseLabel.revive();
            
            this.scale_box.style.display = "block";
        }
    },
    pauseUpdate: function () {
        this.hitSound.volume = this.scale_box.value / 100;
        this.backgroundSound.volume = this.scale_box.value / 100;
    },
    movePlayer: function () {
        this.player.body.velocity.x = 0;
        this.player.body.velocity.y = 0;
        
        if (!this.cursor.left.isDown && !this.cursor.right.isDown && !this.cursor.up.isDown && !this.cursor.down.isDown) {
            if (!this.playerWrap.animations.getAnimation('goFront').isPlaying) {
                this.playerWrap.animations.stop();
            }
            this.playerWrap.animations.play('goFront');
        }

        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -game.global.movementSpeed;
            if (!this.playerWrap.animations.getAnimation('goLeft').isPlaying) {
                this.playerWrap.animations.stop();
            }
            this.playerWrap.animations.play('goLeft');
        }
        if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = game.global.movementSpeed;
            if (!this.playerWrap.animations.getAnimation('goRight').isPlaying) {
                this.playerWrap.animations.stop();
            }
            this.playerWrap.animations.play('goRight');
        }    
        if (this.cursor.up.isDown) {
            this.player.body.velocity.y = -game.global.movementSpeed;
            if (!this.playerWrap.animations.getAnimation('goFront').isPlaying) {
                this.playerWrap.animations.stop();
            }
            this.playerWrap.animations.play('goFront');
        }
        if (this.cursor.down.isDown) {
            this.player.body.velocity.y = game.global.movementSpeed;
            if (!this.playerWrap.animations.getAnimation('goBack').isPlaying) {
                this.playerWrap.animations.stop();
            }
            this.playerWrap.animations.play('goBack');
        }

        if (this.attackKey.isDown) {
            this.player.attack = true;
        }
        else {
            this.player.attack = false;
        }

        this.ultKey.onDown.add(this.playerUlt, this)

        this.playerWrap.x = this.player.x;
        this.playerWrap.y = this.player.y;
        this.playerWrap.body.velocity.x = this.player.body.velocity.x
        this.playerWrap.body.velocity.y = this.player.body.velocity.y

        this.ult.forEach(function (ult, index) {
            if (ult) {
                if (index % 6 === 0) {
                    ult.x = this.player.x + 40;
                    ult.y = this.player.y;
                }
                else if (index % 6 === 1) {
                    ult.x = this.player.x - 40;
                    ult.y = this.player.y;
                }
                else if (index % 6 === 2) {
                    ult.x = this.player.x + 30;
                    ult.y = this.player.y + 30;
                }
                else if (index % 6 === 3) {
                    ult.x = this.player.x - 30;
                    ult.y = this.player.y + 30;
                }
                else if (index % 6 === 4) {
                    ult.x = this.player.x - 30;
                    ult.y = this.player.y - 30;
                }
                else {
                    ult.x = this.player.x + 30;
                    ult.y = this.player.y - 30;
                }
    
                ult.body.velocity.x = this.player.body.velocity.x;
                ult.body.velocity.y = this.player.body.velocity.y;
            }
        }, this)

        /*if (!game.camera.atLimit.y) {
            this.playerLifeLabel.y = 0 + (this.player.y - 250);
            this.enemyLifeLabel.y = 0 + (this.player.y - 250);
        }*/
        
    },
    playerAttack: function () {
        var bullet = game.add.sprite(this.player.x, this.player.y, 'player');
        bullet.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(bullet);
        bullet.body.velocity.y = -1000;

        this.bullet.push(bullet);
    },
    aimbot: function () {
        this.bulletAlt.forEach(function (bullet) {
            if (bullet.body) {
                if (this.enemy && this.enemy.sprite.exists) {
                    var base = Math.sqrt(Math.pow((this.enemy.sprite.y - bullet.y), 2) + Math.pow((this.enemy.sprite.x - bullet.x), 2))
                    //bullet.body.velocity.y = (this.enemy.sprite.y - bullet.y) / base * 500;
                    bullet.body.velocity.x = (this.enemy.sprite.x - bullet.x) / base * 500;
                }
                else if (this.enemyT0[0]) {
                    var base = Math.sqrt(Math.pow((this.enemyT0[0].sprite.y - bullet.y), 2) + Math.pow((this.enemyT0[0].sprite.x - bullet.x), 2))
                    //bullet.body.velocity.y = (this.enemyT0[0].sprite.y - bullet.y) / base * 1000;
                    bullet.body.velocity.x = (this.enemyT0[0].sprite.x - bullet.x) / base * 500;
                }
            }
        }, this)
        if (this.item[0]) {
            this.bullet.forEach(function (bullet) {
                if (bullet.body) {
                    if (this.enemy && this.enemy.sprite.exists) {
                        var base = Math.sqrt(Math.pow((this.enemy.sprite.y - bullet.y), 2) + Math.pow((this.enemy.sprite.x - bullet.x), 2))
                        //bullet.body.velocity.y = (this.enemy.sprite.y - bullet.y) / base * 100;
                        bullet.body.velocity.x = (this.enemy.sprite.x - bullet.x) / base * 1800;
                    }
                    else if (this.enemyT0[0]) {
                        var base = Math.sqrt(Math.pow((this.enemyT0[0].sprite.y - bullet.y), 2) + Math.pow((this.enemyT0[0].sprite.x - bullet.x), 2))
                        //bullet.body.velocity.y = (this.enemyT0[0].sprite.y - bullet.y) / base * 100;
                        bullet.body.velocity.x = (this.enemyT0[0].sprite.x - bullet.x) / base * 1800;
                    }
                }
            }, this)

            this.bulletAlt2.forEach(function (bullet) {
                if (bullet.body) {
                    if (this.enemy && this.enemy.sprite.exists) {
                        var base = Math.sqrt(Math.pow((this.enemy.sprite.y - bullet.y), 2) + Math.pow((this.enemy.sprite.x - bullet.x), 2))
                        //bullet.body.velocity.y = (this.enemy.sprite.y - bullet.y) / base * 500;
                        bullet.body.velocity.x += (this.enemy.sprite.x - bullet.x) / base * 1000;
                    }
                    else if (this.enemyT0[0]) {
                        var base = Math.sqrt(Math.pow((this.enemyT0[0].sprite.y - bullet.y), 2) + Math.pow((this.enemyT0[0].sprite.x - bullet.x), 2))
                        //bullet.body.velocity.y = (this.enemyT0[0].sprite.y - bullet.y) / base * 1000;
                        bullet.body.velocity.x += (this.enemyT0[0].sprite.x - bullet.x) / base * 1000;
                    }
                }
            }, this)
        }
    },
    playerAttackAlt: function () {
        var bullet = game.add.sprite(this.player.x, this.player.y, 'bullet');
        bullet.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(bullet);
        bullet.body.velocity.y = -500;
        bullet.body.velocity.x = (Math.random() - 0.5) * 500;
        
        if (this.enemy && this.enemy.sprite.exists) {
            var base = Math.sqrt(Math.pow((this.enemy.sprite.y - bullet.y), 2) + Math.pow((this.enemy.sprite.x - bullet.x), 2))
            bullet.body.velocity.y = (this.enemy.sprite.y - bullet.y) / base * 500;
            bullet.body.velocity.x = (this.enemy.sprite.x - bullet.x) / base * 500;
        }
        else if (this.enemyT0[0]) {
            var base = Math.sqrt(Math.pow((this.enemyT0[0].sprite.y - bullet.y), 2) + Math.pow((this.enemyT0[0].sprite.x - bullet.x), 2))
            bullet.body.velocity.y = (this.enemyT0[0].sprite.y - bullet.y) / base * 500;
            bullet.body.velocity.x = (this.enemyT0[0].sprite.x - bullet.x) / base * 500;
        }

        this.bulletAlt.push(bullet);
    },
    playerAttackAlt2: function () {
        var bullet = game.add.sprite(this.player.x, this.player.y, 'bullet2');
        bullet.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(bullet);
        bullet.body.velocity.y = -1000;
        bullet.body.velocity.x = Math.random() * 500;

        var bullet2 = game.add.sprite(this.player.x, this.player.y, 'bullet2');
        bullet2.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(bullet2);
        bullet2.body.velocity.y = -1000;
        bullet2.body.velocity.x = -Math.random() * 500;

        this.bulletAlt2.push(bullet);
        this.bulletAlt2.push(bullet2);
    },
    playerUlt: function () {
        if (this.player.ult) {
            var ult = game.add.sprite(this.player.x + 40, this.player.y, 'ult');
            ult.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(ult);
            //ult.body.velocity.y = -2000;
            this.ult.push(ult);

            ult = game.add.sprite(this.player.x - 40, this.player.y, 'ult');
            ult.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(ult);
            //ult.body.velocity.y = -2000;
            this.ult.push(ult);

            ult = game.add.sprite(this.player.x + 30, this.player.y + 30, 'ult');
            ult.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(ult);
            //ult.body.velocity.y = -2000;
            this.ult.push(ult);

            ult = game.add.sprite(this.player.x - 30, this.player.y + 30, 'ult');
            ult.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(ult);
            //ult.body.velocity.y = -2000;
            this.ult.push(ult);

            ult = game.add.sprite(this.player.x - 30, this.player.y - 30, 'ult');
            ult.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(ult);
            //ult.body.velocity.y = -2000;
            this.ult.push(ult);

            ult = game.add.sprite(this.player.x + 30, this.player.y - 30, 'ult');
            ult.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(ult);
            //ult.body.velocity.y = -2000;
            this.ult.push(ult);

            this.player.ult -= 1;
        }
    },
    playerDie: function () {
        game.global.score = 9999 + this.timer - this.lvltime + this.player.life * 5000;
        game.state.start('fail');
        this.backgroundSound.stop();
    },
    playerWin: function () {
        game.global.score = 9999 + this.timer - this.lvltime + this.player.life * 5000;
        game.state.start('win');
        this.backgroundSound.stop();
    },
    enemyCreate: function () {
        this.enemy = new enemyObj();
        this.enemy.sprite = game.add.sprite(game.width/2, 0, 'enemy');
        this.enemy.sprite.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.enemy.sprite);
        this.enemy.sprite.body.collideWorldBounds = true;
        this.enemy.life = 100;

        this.enemy.wrap = game.add.sprite(game.width/2, 0, 'enemyWrap');
        this.enemy.wrap.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.enemy.wrap);
        this.enemy.wrap.frame = 0;
        this.enemy.wrap.animations.add('rotate', [0, 1, 2, 3, 4], 8, true);
        this.enemy.wrap.animations.play('rotate');

        game.add.tween(this.enemy.sprite).to({y: game.height/4}, 1000).start();
        game.add.tween(this.enemy.wrap).to({y: game.height/4}, 1000).start();
    },
    moveEnemy: function () {
        this.enemy.sprite.body.velocity.x = (Math.random() - 0.5) * 1000;

        this.enemy.wrap.x = this.enemy.sprite.x;
        this.enemy.wrap.y = this.enemy.sprite.y;
        this.enemy.wrap.body.velocity.x = this.enemy.sprite.body.velocity.x;
        this.enemy.wrap.body.velocity.y = this.enemy.sprite.body.velocity.y;

        this.enemy.sprite.body.onWorldBounds = new Phaser.Signal();
        this.enemy.sprite.body.onWorldBounds.add(this.wrapHandle, this.enemy);
    },
    enemyAttack: function () {
        var danmaku = game.add.sprite(this.enemy.sprite.x, this.enemy.sprite.y, 'danmaku');
        danmaku.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(danmaku);

        var period = game.time.now * 0.01;
        danmaku.body.velocity.x = Math.cos(period) * 100;
        danmaku.body.velocity.y = Math.sin(period) * 100;

        this.enemy.danmaku.push(danmaku);
    },
    enemyDanmakuMove: function () {
        /*var period = game.time.now * 0.005;
        this.enemy.danmaku.forEach(function (danmaku, index) {
            danmaku.body.velocity.x = Math.cos(period) * 10;
            danmaku.body.velocity.y = Math.sin(period) * 10;

            if (!danmaku.inWorld) {
                this.enemy.danmaku.splice(index, 1);
            }
        }, this)*/
    },
    destroyEnemy: function () {
        this.enemy.danmaku.forEach(function (danmaku) {
            danmaku.destroy();
        })
        this.enemy.danmaku = [];
        if (this.enemy.sprite.exists) {
            //this.particleEnemy();
            this.enemy.sprite.kill();
            this.enemy.wrap.kill();
        }
    },
    particleEnemy: function () {
        this.emitter.x = this.enemy.sprite.x;
        this.emitter.y = this.enemy.sprite.y;
        this.emitter.start(true, 500, null, 10);
    },
    enemyCreateT0: function () {
        var Enemy = new enemyObj();
        Enemy.sprite = game.add.sprite(game.width / 2, 0, 'enemyT0');
        Enemy.sprite.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(Enemy.sprite);
        Enemy.life = 3;

        Enemy.wrap = game.add.sprite(game.width/2, 0, 'enemyT0Wrap');
        Enemy.wrap.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(Enemy.wrap);

        this.enemyT0.push(Enemy);
    },
    moveEnemyT0: function () {
        var period = game.time.now * 0.001;
        this.enemyT0.forEach(function (Enemy, index) {
            Enemy.sprite.body.velocity.x += Math.cos(period);
            Enemy.sprite.body.velocity.y += Math.abs(Math.sin(period) * 5)

            Enemy.wrap.x = Enemy.sprite.x;
            Enemy.wrap.y = Enemy.sprite.y;
            Enemy.wrap.body.velocity.x = Enemy.sprite.body.velocity.x;
            Enemy.wrap.body.velocity.y = Enemy.sprite.body.velocity.y;

            Enemy.sprite.body.onWorldBounds = new Phaser.Signal();
            Enemy.sprite.body.onWorldBounds.add(this.wrapHandle, Enemy);

            if (!Enemy.sprite.inWorld) {
                this.destroyEnemyT0(Enemy);
                Enemy.sprite.destroy();
                Enemy.wrap.destroy();
                this.enemyT0.splice(index, 1);
            }
        }, this)
    },
    enemyAttackT0: function (target) {
        var danmaku = game.add.sprite(target.sprite.x, target.sprite.y, 'danmaku2');
        danmaku.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(danmaku);
        danmaku.body.velocity.y = (this.player.y - target.sprite.y);
        danmaku.body.velocity.x = (this.player.x - target.sprite.x);
        target.danmaku.push(danmaku);
    },
    destroyEnemyT0: function (target) {
        target.danmaku.forEach(function (danmaku) {
            danmaku.destroy();
        })
        target.danmaku = [];
        if (target.sprite.exists) {
            this.particleEnemyT0(target);
            target.sprite.kill();
            target.wrap.kill();
        }
    },
    particleEnemyT0: function (target) {
        this.emitter.x = target.sprite.x;
        this.emitter.y = target.sprite.y;
        this.emitter.start(true, 500, null, 10);
    },
    wrapHandle: function () {
        this.wrap.x = this.sprite.x;
        this.wrap.y = this.sprite.y;
        this.wrap.body.velocity.x = 0;
        this.wrap.body.velocity.y = 0;
    }
};