// Initialize Phaser
var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = {
    // whatever needed
    movementSpeed: 400,
    score: 0
};
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('fail', failState);
game.state.add('win', winState);
// Start the 'boot' state
game.state.start('boot');